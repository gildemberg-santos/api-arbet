from src.dao.mongodao import MongoDao as md
from src.json.salvarjson import SalvarJson as sj

class Salvar(object):
    def __init__(self):
        pass

    def odds(self, data=None):
        """
        data="2020-08-12"
        """
        url = "https://api-football-beta.p.rapidapi.com/odds"
        querystring = {}
        if data:
            querystring = {"date":data}
        j = sj()
        r = j.download(url=url, querystring=querystring)
        j.writejson(dados=r.content, nome='odds')
        r = j.readjson(nome='odds')
        for item in r['response']:
            dao = md()
            filter = {"league.id": item['league']['id'], "fixture.id": item['fixture']['id']}
            dao.upsert(tb=dao.cl_odd, d_json=item, filter=filter)

    def seasons(self):
        url = "https://api-football-beta.p.rapidapi.com/leagues/seasons"
        j = sj()
        r = j.download(url=url)
        j.writejson(dados=r.content, nome='seasons')
        r = j.readjson(nome='seasons')
        for item in r['response']:
            dao = md()
            filter = {"year": item}
            dao.upsert(tb=dao.cl_seasons, d_json={"year": item}, filter=filter)
    
    def leagues(self):
        url = "https://api-football-beta.p.rapidapi.com/leagues"
        j = sj()
        r = j.download(url=url)
        j.writejson(dados=r.content, nome='leagues')
        r = j.readjson(nome='leagues')
        for item in r['response']:
            dao = md()
            filter = {"league.id": item['league']['id']}
            dao.upsert(tb=dao.cl_leagues, d_json=item, filter=filter)

    def fixtures(self, data=None):
        """
        data="2020-08-12"
        """
        url = "https://api-football-beta.p.rapidapi.com/fixtures"
        querystring = {}
        if data:
            querystring = {"date":data}
        j = sj()
        r = j.download(url=url, querystring=querystring)
        j.writejson(dados=r.content, nome='fixtures')
        r = j.readjson(nome='fixtures')
        for item in r['response']:
            dao = md()
            filter = {"fixture.id": item['fixture']['id']}
            dao.upsert(tb=dao.cl_fixtures, d_json=item, filter=filter)

    def teams(self, league=None, season=None):
        url = "https://api-football-beta.p.rapidapi.com/teams"
        querystring = {}
        if season and league:
            querystring = {"league":league, "season":season} # "league":"4","season":"2020"
        j = sj()
        r = j.download(url=url, querystring=querystring)
        j.writejson(dados=r.content, nome='teams')
        r = j.readjson(nome='teams')
        for item in r['response']:
            dao = md()
            filter = {"team.id": item['team']['id']}
            dao.upsert(tb=dao.cl_teams, d_json=item, filter=filter)

    def countries(self):
        url = "https://api-football-beta.p.rapidapi.com/countries"
        j = sj()
        r = j.download(url=url)
        j.writejson(dados=r.content, nome='countries')
        r = j.readjson(nome='countries')
        for item in r['response']:
            dao = md()
            filter = {"code" : item['code']}
            dao.upsert(tb=dao.cl_countries, d_json=item, filter=filter)

    def timezone(self):
        url = "https://api-football-beta.p.rapidapi.com/timezone"
        j = sj()
        r = j.download(url=url)
        j.writejson(dados=r.content, nome='timezone')
        r = j.readjson(nome='timezone')
        for item in r['response']:
            dao = md()
            filter = {"timezone" : item}
            dao.upsert(tb=dao.cl_timezone, d_json={"timezone" : item}, filter=filter)