from src.dao.mongodao import MongoDao as md
import pandas as pd


class Ler(object):
    def __init__(self):
        pass

    def handicap(self, home, away):
        for h, a in zip(home, away):
            pass

    def values(self, df=pd.DataFrame(), nome=None):
        df = pd.DataFrame(df)
        lista = []
        for v, o in zip(df['value'], df['odd']):
            name, valor = str(v).split(" ")[0], float(str(v).split(" ")[1])
            if name == nome:
                lista.append([name, valor, float(o)])
            del v, o
        return lista

    def bets(self, df=pd.DataFrame()):
        df = pd.DataFrame(df)
        df = df.loc[df['id']==4]
        for i, n, v in zip(df['id'], df['name'], df['values']):
            print(n)
            print(self.values(v, nome="Home"))
            print(self.values(v, nome="Away"))
            print('*'*50)
            del i, n, v

    def bookmakers(self, df=pd.DataFrame()):
        df = pd.DataFrame(df)
        for i, n, b in zip(df['id'], df['name'], df['bets']):
            self.bets(b)
            del i, n, b

    def odd(self, idfixture=None, idbookmakers=None):
        d = md()
        if idfixture:
            filter = {'fixture.id': idfixture}
        else:
            filter = {}
        r = d.find(tb=d.cl_odd, filter=filter)
        df = pd.DataFrame(data=r)
        for f, l, bm in zip(df['fixture'], df['league'], df['bookmakers']):
            self.bookmakers(bm)
            del f, l, bm
