import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio


class WindowsGTK(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Sem titulo")
        self.set_size_request(800, 600)
        self.set_border_width(6)

        header = Gtk.HeaderBar(title="Arbet")
        header.set_subtitle("Sistema de Apostas Esportivas")
        header.props.show_close_button = True
        button = Gtk.Button()
        button.connect("clicked", self.on_button_clicked)
        icon = Gio.ThemedIcon(name="mail-send-receive-symbolic")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        header.pack_end(button)
        self.set_titlebar(header)

        
        self.boxh = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.label1 = Gtk.Label(label="Você e um programado python?", xalign=0)
        self.boxh.pack_start(self.label1, True, True, 0)
        self.switch = Gtk.Switch()
        self.switch.props.valign = Gtk.Align.CENTER
        self.boxh.pack_start(self.switch, False, True, 0)

        self.boxh2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.label2 = Gtk.Label(label="Nome:", xalign=0)
        self.boxh2.pack_start(self.label2, False, True, 0)
        self.entry = Gtk.Entry()
        self.entry.set_text("")
        self.boxh2.pack_start(self.entry, True, True, 0)

        self.boxv = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.boxv)
        self.boxv.pack_start(self.boxh, False, True, 0)
        self.boxv.pack_start(self.boxh2, False, True, 0)

        self.button1 = Gtk.Button(label="Hello")
        self.button1.connect("clicked", self.on_button1_clicked)
        self.boxv.pack_start(self.button1, False, True, 0)

        self.button2 = Gtk.Button(label="Goodbye")
        self.button2.connect("clicked", self.on_button2_clicked)
        self.boxv.pack_start(self.button2, False, True, 0)

    def on_button_clicked(self, widget):
        print("Title")
    
    def on_button1_clicked(self, widget):
        print("Hello")

    def on_button2_clicked(self, widget):
        print("Goodbye")
    

win = WindowsGTK()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()