import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from src.dao.mongodao import MongoDao


class Cadastro(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="")
        self.set_position(position=Gtk.WindowPosition.CENTER)
        self.set_size_request(800, 450)
        self.set_border_width(6)

        # Titulo
        self.hbTitulo = Gtk.HeaderBar(title="Cadastro")
        self.hbTitulo.props.show_close_button = True
        self.set_titlebar(self.hbTitulo)

        # Nome
        self.bxNome = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblNome = Gtk.Label(label="Nome:", xalign=0)
        self.bxNome.pack_start(self.lblNome, False, True, 0)
        self.entryNome = Gtk.Entry()
        self.entryNome.set_text("")
        self.bxNome.pack_start(self.entryNome, True, True, 0)
        

        # Sobrenome
        self.bxSobrenome = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblSobrenome = Gtk.Label(label="Sobrenome:", xalign=0)
        self.bxSobrenome.pack_start(self.lblSobrenome, False, True, 0)
        self.entrySobrenome = Gtk.Entry()
        self.entrySobrenome.set_text("")
        self.bxSobrenome.pack_start(self.entrySobrenome, True, True, 0)
        

        # Endereço
        self.bxEndereco = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblEndereco = Gtk.Label(label="Endereço:", xalign=0)
        self.bxEndereco.pack_start(self.lblEndereco, False, True, 0)
        self.entryEndereco = Gtk.Entry()
        self.entryEndereco.set_text("")
        self.bxEndereco.pack_start(self.entryEndereco, True, True, 0)



        # Telefone
        self.bxTelefone = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblTelefone = Gtk.Label(label="Telefone:", xalign=0)
        self.bxTelefone.pack_start(self.lblTelefone, False, True, 0)
        self.entryTelefone = Gtk.Entry()
        self.entryTelefone.set_text("")
        self.bxTelefone.pack_start(self.entryTelefone, True, True, 0)


        # Login
        self.bxLogin = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblLogin = Gtk.Label(label="Login:", xalign=0)
        self.bxLogin.pack_start(self.lblLogin, False, True, 0)
        self.entryLogin = Gtk.Entry()
        self.entryLogin.set_text("")
        self.bxLogin.pack_start(self.entryLogin, True, True, 0)

        # Senha
        self.bxSenha = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblSenha = Gtk.Label(label="Senha:", xalign=0)
        self.bxSenha.pack_start(self.lblSenha, False, True, 0)
        self.entrySenha = Gtk.Entry()
        self.entrySenha.set_visibility(False)
        self.entrySenha.set_text("")
        self.bxSenha.pack_start(self.entrySenha, True, True, 0)

        #Permanece logado
        self.bxMnLogin = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.lblMnLogin = Gtk.Label(label="", xalign=0)
        self.lblMnLogin.set_max_width_chars(15)
        self.bxMnLogin.pack_start(self.lblMnLogin, True, True, 0)

        #Permanece logado
        self.bxActionCadastrar = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.btnCadastrar = Gtk.Button(label="Cadastrar")
        self.btnCadastrar.connect("clicked", self.cadastrar_button_clicked)
        self.bxActionCadastrar.pack_start(self.btnCadastrar, False, True, 0)

        # Box Tela Login
        self.bxTela = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.bxTela.pack_start(self.bxNome, False, True, 0)
        self.bxTela.pack_start(self.bxSobrenome, False, True, 0)
        self.bxTela.pack_start(self.bxEndereco, False, True, 0)
        self.bxTela.pack_start(self.bxTelefone, False, True, 0)
        self.bxTela.pack_start(self.bxLogin, False, True, 0)
        self.bxTela.pack_start(self.bxSenha, False, True, 0)
        self.bxTela.pack_start(self.bxMnLogin, False, True, 0)
        self.bxTela.pack_start(self.bxActionCadastrar, False, True, 0)
        self.add(self.bxTela)

    def per_logado_toggled(self, button):
        if button.get_active():
            print("Manter logado!")
        else:
            print("Não vai manter logado!")
    
    def cadastrar_button_clicked(self, button):
        if self.entryNome != "" and self.entrySobrenome != "":
            print("Logado")
            self.lblMnLogin.set_text("Logado")
            dao = MongoDao()
            valores = {
                "nome":self.entryNome.get_text(),
                "sobrenome":self.entrySobrenome.get_text(),
                "endereco":self.entryEndereco.get_text()
                }
            dao.insert(tb=dao.cl_usuarios, d_json=valores)
        else:
            self.lblMnLogin.set_text("Login ou Senha invalidos!")
            print("Login ou Senha invalidos!")


win = Cadastro()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()