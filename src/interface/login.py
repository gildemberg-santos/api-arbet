import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Login(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="")
        self.set_position(position=Gtk.WindowPosition.CENTER)
        self.set_size_request(400, 250)
        self.set_border_width(6)

        # Titulo
        self.hbTitulo = Gtk.HeaderBar(title="Login")
        self.hbTitulo.props.show_close_button = True
        self.set_titlebar(self.hbTitulo)

        # Login
        self.bxLogin = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblLogin = Gtk.Label(label="Login:", xalign=0)
        self.bxLogin.pack_start(self.lblLogin, False, True, 0)
        self.entryLogin = Gtk.Entry()
        self.entryLogin.set_text("")
        self.bxLogin.pack_start(self.entryLogin, True, True, 0)

        # Senha
        self.bxSenha = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.lblSenha = Gtk.Label(label="Senha:", xalign=0)
        self.bxSenha.pack_start(self.lblSenha, False, True, 0)
        self.entrySenha = Gtk.Entry()
        self.entrySenha.set_visibility(False)
        self.entrySenha.set_text("")
        self.bxSenha.pack_start(self.entrySenha, True, True, 0)

        #Permanece logado
        self.bxMnLogin = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.lblMnLogin = Gtk.Label(label="", xalign=0)
        self.lblMnLogin.set_max_width_chars(15)
        self.bxMnLogin.pack_start(self.lblMnLogin, True, True, 0)
        self.ckMnLogin = Gtk.CheckButton(label="Continuar conectado!")
        self.ckMnLogin.connect("toggled", self.per_logado_toggled)
        self.ckMnLogin.set_active(False)
        self.bxMnLogin.pack_start(self.ckMnLogin, False, True, 0)

        #Permanece logado
        self.bxActionLogin = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.btnLogin = Gtk.Button(label="Fazer login")
        self.btnLogin.connect("clicked", self.logado_button_clicked)
        self.bxActionLogin.pack_start(self.btnLogin, False, True, 0)

        # Box Tela Login
        self.bxTela = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.bxTela.pack_start(self.bxLogin, False, True, 0)
        self.bxTela.pack_start(self.bxSenha, False, True, 0)
        self.bxTela.pack_start(self.bxMnLogin, False, True, 0)
        self.bxTela.pack_start(self.bxActionLogin, False, True, 0)
        self.add(self.bxTela)

    def per_logado_toggled(self, button):
        if button.get_active():
            print("Manter logado!")
        else:
            print("Não vai manter logado!")
    
    def logado_button_clicked(self, button):
        if (self.entryLogin.get_text() != "" and self.entrySenha.get_text() != "") and (self.entryLogin.get_text() == "gildemberg.santos" and self.entrySenha.get_text() == "@Gsx1300r"):
            print("Logado")
            self.lblMnLogin.set_text("Logado")
        else:
            self.lblMnLogin.set_text("Login ou Senha invalidos!")
            print("Login ou Senha invalidos!")


win = Login()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()