from pymongo import MongoClient


class Connection(object): # Connection('localhost', 27017)
    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port

class ConnectionUrl: # mongodb+srv://gildemberg:%40Gsx1300r@cluster0.ujnh5.gcp.mongodb.net/test?retryWrites=true&w=majority
    def __init__(self, _user=None, _password=None, _database=None):
        self.user = _user
        self.password = _password
        self.database = _database
        url = "mongodb+srv://{0}:{1}@cluster0.ujnh5.gcp.mongodb.net/{2}?retryWrites=true&w=majority".format(self.user, self.password, self.database)
        self.urlconn = url

class MongoDao(object):
    def __init__(self):
        conn = ConnectionUrl(_user="gildemberg", _password="gsx1300r", _database="test")
        cliente = MongoClient(conn.urlconn)
        self.banco = cliente[conn.database]
        self.cl_odd = self.banco.Odd
        self.cl_seasons = self.banco.Seasons
        self.cl_leagues = self.banco.Leagues
        self.cl_fixtures = self.banco.Fixtures
        self.cl_teams = self.banco.Teams
        self.cl_countries = self.banco.Countries
        self.cl_timezone = self.banco.Timezone
        # Dados Pessoais
        self.cl_usuarios = self.banco.Usuarios

    def insert(self, tb, d_json):
        tb.insert_one(d_json)

    def update(self, tb, d_json, filter=None):
        tb.update_one(filter=filter, update={"$set": d_json}, upsert=False)

    def upsert(self, tb, d_json, filter=None):
        tb.update_many(filter=filter, update={"$set": d_json}, upsert=True)

    def delete(self, tb, filter=None):
        tb.delete_many(filter=filter)

    def find_one(self, tb, filter=None):
        d_json = tb.find_one(filter=filter)
        return d_json
    
    def find(self, tb, filter=None):
        d_json = tb.find(filter=filter)
        return d_json

    def find_count(self, tb, filter=None):
        d_json = tb.find(filter=filter).count()
        return d_json
    
    def find_distinct(self, tb, distinct=None):
        d_json = tb.distinct(distinct)
        return d_json
    
    

