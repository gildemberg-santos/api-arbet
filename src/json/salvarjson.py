import requests
import json
import os


class SalvarJson(object):
    def __init__(self):
        pass

    def download(self, url='', method="GET", payload={}, querystring={}):
        try:
            headers = {
                'x-rapidapi-host': "api-football-beta.p.rapidapi.com",
                'x-rapidapi-key': "9723222928msh0846b451188c36ap1140d9jsn201d5b8aa982",
                'accept-encoding': "gzip"
            }
            response = requests.request(method=method, url=url, data=payload, params=querystring, headers=headers)
            if response.status_code == 200:
                return response
        except Exception as e:
            print('Erro: ', e.__context__)
            return None

    def writejson(self, dados='', nome=''):
        try:
            os.mkdir('temp')
        except:
            pass

        try:
            with open("{0}{1}.{2}".format('temp/', nome, 'json'), 'w') as fdw:
                json.dump(json.loads(dados), fdw, indent=4)
        except:
            pass

    def readjson(self, nome=''):
        dados = None
        try:
            with open("{0}{1}.{2}".format('temp/', nome, 'json'), 'r') as fdr:
                dados = fdr.read()
        except:
            pass
        return json.loads(dados)
