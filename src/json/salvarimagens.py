import requests
import json
import os


class SalvarImagens(object):
    def __init__(self):
        pass

    def download(self, url='', method="GET", payload={}, querystring={}):
        try:
            headers = {
                'accept-encoding': "gzip"
            }
            response = requests.request(method=method, url=url, data=payload, params=querystring, headers=headers)
            if response.status_code == 200:
                return response
        except Exception as e:
            print('Erro: ', e.__context__)
            return None

    def write(self, dados='', nome='', formato='svg'):
        try:
            os.mkdir('img')
        except:
            pass
        try:
            with open("{0}{1}.{2}".format('img/', nome, formato), 'wb') as fdw:
                for dado in dados.iter_content(1024):
                    if not dado:
                        break
                    fdw.write(dado)
        except Exception as e:
            print('Erro: ', e.__context__)